package com.mohammad.tvmazeapp.di

import com.mohammad.tvmazeapp.presentation.details.DetailsActivity
import com.mohammad.tvmazeapp.presentation.details.DetailsModule
import com.mohammad.tvmazeapp.presentation.main.MainActivity
import com.mohammad.tvmazeapp.presentation.main.MainModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [MainModule::class])
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [DetailsModule::class])
    abstract fun bindDetailsActivity(): DetailsActivity
}