package com.mohammad.tvmazeapp.di

import com.mohammad.tvmazeapp.Constants
import com.mohammad.tvmazeapp.domain.IContentManager
import com.mohammad.tvmazeapp.domain.ILogger
import com.mohammad.tvmazeapp.framework.ContentManager
import com.mohammad.tvmazeapp.framework.Logger
import com.mohammad.tvmazeapp.framework.Navigator
import com.mohammad.tvmazeapp.framework.api.TvMazeService
import dagger.Module
import dagger.Provides
import dagger.Reusable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module(includes = [BaseModule::class])
class AppModule {

    @Reusable
    @Provides
    fun provideMarvelApi(): TvMazeService {
        return Retrofit.Builder()
            .baseUrl(Constants.TvMaze_API)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build().create(TvMazeService::class.java)
    }

    @Provides
    fun provideNavigator(): Navigator = Navigator()


    @Provides
    fun provideContentManager(tvMazeService: TvMazeService): IContentManager = ContentManager(tvMazeService)

    @Provides
    fun provideLogger(): ILogger = Logger()
}