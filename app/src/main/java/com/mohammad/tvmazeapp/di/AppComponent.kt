package com.mohammad.tvmazeapp.di

import android.app.Application
import com.mohammad.tvmazeapp.presentation.TvMazeApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@Component(modules = [ActivityBuilder::class,AppModule::class,AndroidSupportInjectionModule::class])
interface AppComponent: AndroidInjector<TvMazeApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: Application): Builder
        fun appModule(appModule: AppModule): Builder
        fun build(): AppComponent
    }
}