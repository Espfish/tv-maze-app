package com.mohammad.tvmazeapp.framework

import com.mohammad.tvmazeapp.domain.ILogger
import timber.log.Timber

class Logger: ILogger {
    override fun d(msg: String) {
        Timber.d(msg)
    }

    override fun d(t: Throwable) {
        Timber.d(t)
    }

    override fun e(msg: String) {
        Timber.e(msg)
    }

    override fun e(t: Throwable) {
        Timber.e(t)
    }
}