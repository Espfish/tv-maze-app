package com.mohammad.tvmazeapp.framework

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.mohammad.tvmazeapp.presentation._common.models.Show
import com.mohammad.tvmazeapp.presentation.details.DetailsActivity

class Navigator {

    fun navigateToDetailsActivity(context: Context,show:Show, shouldFinish: Boolean){
        startActivity(context,DetailsActivity.newInstance(context,show),shouldFinish)
    }

    private fun startActivity(context: Context,intent: Intent, shouldFinish: Boolean){
        context.startActivity(intent)
        if(shouldFinish && context is Activity){
            context.finish()
        }
    }
}