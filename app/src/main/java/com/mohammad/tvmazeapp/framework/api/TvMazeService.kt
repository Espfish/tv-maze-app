package com.mohammad.tvmazeapp.framework.api

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface TvMazeService {
    @GET("/search/shows")// /search/shows?q=:query
    fun searchShowsByName(@Query("q") q: String): Single<List<ApiWrapper>>

    @GET("/schedule")// /schedule?country=:countrycode&date=:date
    fun getShowsForDate(@Query("country") countrycode: String, @Query("date") date: String): Single<List<ApiWrapper>>

}