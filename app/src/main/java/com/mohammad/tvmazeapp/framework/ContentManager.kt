package com.mohammad.tvmazeapp.framework

import com.mohammad.tvmazeapp.domain.IContentManager
import com.mohammad.tvmazeapp.framework.api.TvMazeService
import com.mohammad.tvmazeapp.framework.api.ApiWrapper
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ContentManager(private val tvMazeService: TvMazeService): IContentManager {
    override fun getShowsForDate(date: String, listener: SingleObserver<List<ApiWrapper>>){
        subscribe(tvMazeService.getShowsForDate("US",date), listener)
    }

    override fun searchShowByName(q: String, listener: SingleObserver<List<ApiWrapper>>){
        subscribe(tvMazeService.searchShowsByName(q), listener)
    }

    private fun <T> subscribe(single: Single<T>, listener: SingleObserver<T>) {
        single.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(listener)
    }
}