package com.mohammad.tvmazeapp.framework.api

class ApiWrapper(val show: ApiShow)
class ApiShow(val name: String?, val image: ApiImage?, val summary: String?)
class ApiImage(val medium: String?)