package com.mohammad.tvmazeapp.presentation.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.mohammad.tvmazeapp.R
import com.mohammad.tvmazeapp.presentation._common.BaseActivity
import com.mohammad.tvmazeapp.presentation._common.models.Show
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_details.*
import javax.inject.Inject

class DetailsActivity : BaseActivity(), DetailsContract.View {

    @Inject
    lateinit var presenter: DetailsContract.Presenter



    companion object{
        const val SHOW = "show"
        fun newInstance(context: Context,show: Show): Intent{
            val intent = Intent(context,DetailsActivity::class.java)
            intent.putExtra(SHOW,show)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        presenter.onCreate(intent.getParcelableExtra(SHOW))
    }

    override fun initLayout(show: Show) {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            title = show.name
            setDefaultDisplayHomeAsUpEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }

        Picasso.get().load(show.image?.medium).into(showImage)
        showDesription.text = show.summary
    }
}
