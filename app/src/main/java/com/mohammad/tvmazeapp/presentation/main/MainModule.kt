package com.mohammad.tvmazeapp.presentation.main

import com.mohammad.tvmazeapp.domain.IContentManager
import com.mohammad.tvmazeapp.domain.ILogger
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module(includes = [MainProviders::class])
abstract class MainModule {

    @Binds
    abstract fun bindView(activity: MainActivity): MainContract.View

}

@Module
object MainProviders{

    @Provides
    fun providePresenter(view: MainContract.View, interactor: MainContract.Interactor, logger:ILogger): MainContract.Presenter = MainPresenterImpl(view,interactor,logger)

    @Provides
    fun provideInteractor(contentManager: IContentManager): MainContract.Interactor = MainInteractor(contentManager)
}