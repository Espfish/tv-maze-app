package com.mohammad.tvmazeapp.presentation.main

import com.mohammad.tvmazeapp.domain.ILogger
import com.mohammad.tvmazeapp.framework.api.ApiShow
import com.mohammad.tvmazeapp.framework.api.ApiWrapper
import com.mohammad.tvmazeapp.presentation._common.models.Show
import com.mohammad.tvmazeapp.presentation._common.models.toShow
import com.mohammad.tvmazeapp.presentation._common.rxjava.ApiListener
import io.reactivex.disposables.CompositeDisposable
import java.text.FieldPosition
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class MainPresenterImpl @Inject constructor(val view: MainContract.View, val interactor: MainContract.Interactor, val logger: ILogger): MainContract.Presenter {

    var shows : List<Show> = listOf()

    private val compositeDisposable = CompositeDisposable()

    var search = ""

    override fun onCreate() {
        view.initLayout()
        bindTodaysShows()
    }


    override fun onSearch(search:String) {
        this.search = search
        view.showLoad()
        val listener = object : ApiListener<List<ApiWrapper>>() {
            override fun onSuccess(wrappers: List<ApiWrapper>) {
                shows = wrappers.map { it.show.toShow() }
                view.showData()
                view.loadData(shows)
            }

            override fun onError(e: Throwable) {
                view.showNoNetwork()
                logger.e(e)
            }

        }
        compositeDisposable.add(listener)
        interactor.searchShow(search,listener)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
    }

    override fun onItemClicked(position: Int) {
        view.navigateToDetailsScreen(shows[position])
    }

    override fun bindTodaysShows() {
        search = ""
        view.showLoad()
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val today = sdf.format(Date())
        val listener = object : ApiListener<List<ApiWrapper>>() {
            override fun onSuccess(wrappers: List<ApiWrapper>) {
                shows = wrappers.map { it.show.toShow() }
                view.showData()
                view.loadData(shows)
            }

            override fun onError(e: Throwable) {
                logger.e(e)
                view.showNoNetwork()
            }

        }
        compositeDisposable.add(listener)
        interactor.getShowsForDate(today,listener)
    }

    override fun retryClicked() {
        if(search.isBlank()){
            bindTodaysShows()
        }else{
            onSearch(search)
        }
    }
}