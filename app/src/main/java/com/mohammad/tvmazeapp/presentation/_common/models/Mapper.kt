package com.mohammad.tvmazeapp.presentation._common.models

import androidx.core.text.HtmlCompat
import com.mohammad.tvmazeapp.framework.api.ApiImage
import com.mohammad.tvmazeapp.framework.api.ApiShow

fun ApiShow.toShow() = Show(name,image?.toImage(),HtmlCompat.fromHtml(summary?:"", HtmlCompat.FROM_HTML_MODE_LEGACY).toString())
fun ApiImage.toImage() = Image(medium)