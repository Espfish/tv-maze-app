package com.mohammad.tvmazeapp.presentation.details

import com.mohammad.tvmazeapp.domain.ILogger
import com.mohammad.tvmazeapp.presentation._common.models.Show
import javax.inject.Inject

class DetailsPresenterImpl @Inject constructor(val view: DetailsContract.View, val logger: ILogger): DetailsContract.Presenter {

    var show: Show? = null

    override fun onCreate(show: Show) {
        this.show = show
        this.show?.let {
            view.initLayout(it)
        }

    }
}