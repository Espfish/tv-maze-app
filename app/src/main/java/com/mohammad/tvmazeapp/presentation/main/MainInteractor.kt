package com.mohammad.tvmazeapp.presentation.main

import com.mohammad.tvmazeapp.domain.IContentManager
import com.mohammad.tvmazeapp.framework.api.ApiWrapper
import com.mohammad.tvmazeapp.presentation._common.rxjava.ApiListener
import javax.inject.Inject

class MainInteractor @Inject constructor(val contentManager: IContentManager): MainContract.Interactor {
    override fun getShowsForDate(date: String, listener:  ApiListener<List<ApiWrapper>>) {
        contentManager.getShowsForDate(date,listener)
    }

    override fun searchShow(search: String,listener:  ApiListener<List<ApiWrapper>>) {
        contentManager.searchShowByName(search,listener)
    }
}