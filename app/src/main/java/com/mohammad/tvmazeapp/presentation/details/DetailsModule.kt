package com.mohammad.tvmazeapp.presentation.details

import com.mohammad.tvmazeapp.domain.IContentManager
import com.mohammad.tvmazeapp.domain.ILogger
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module(includes = [DetailsProviders::class])
abstract class DetailsModule {

    @Binds
    abstract fun bindView(activity: DetailsActivity): DetailsContract.View

}

@Module
object DetailsProviders{

    @Provides
    fun providePresenter(view: DetailsContract.View, logger:ILogger): DetailsContract.Presenter = DetailsPresenterImpl(view,logger)
}