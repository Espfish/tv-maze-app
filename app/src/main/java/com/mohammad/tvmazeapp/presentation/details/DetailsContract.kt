package com.mohammad.tvmazeapp.presentation.details

import com.mohammad.tvmazeapp.presentation._common.models.Show

interface DetailsContract {
    interface View{
        fun initLayout(show: Show)

    }

    interface Presenter{
        fun onCreate(show: Show)
    }
}