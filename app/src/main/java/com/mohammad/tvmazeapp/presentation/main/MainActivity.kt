package com.mohammad.tvmazeapp.presentation.main

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.core.view.MenuItemCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.mohammad.tvmazeapp.R
import com.mohammad.tvmazeapp.framework.api.ApiShow
import com.mohammad.tvmazeapp.presentation._common.BaseActivity
import com.mohammad.tvmazeapp.presentation._common.models.Show
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.no_network_connection.*
import java.text.FieldPosition
import javax.inject.Inject

class MainActivity : BaseActivity(), MainContract.View {

    @Inject
    lateinit var presenter: MainContract.Presenter

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)

        val searchItem: MenuItem? = menu?.findItem(R.id.action_search)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView: SearchView? = searchItem?.actionView as SearchView
        searchView?.apply {
            setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    presenter.onSearch(query?:"")
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean = true

            })
        }
        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener{
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean = true

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                presenter.bindTodaysShows()
                return true
            }

        })

        searchView?.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        return super.onCreateOptionsMenu(menu)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.onCreate()
    }

    override fun initLayout() {
        setSupportActionBar(toolbar)
        retryButton.setOnClickListener {
            presenter.retryClicked()
        }
    }

    override fun loadData(shows: List<Show>) {
        tvMazeShows.adapter = TvShowsAdapter(shows, object : TvShowsAdapter.TvShowsAdapterInterface{
            override fun onItemClick(position: Int) {
                presenter.onItemClicked(position)
            }

        })
        tvMazeShows.layoutManager = LinearLayoutManager(this)
    }

    override fun navigateToDetailsScreen(show: Show) {
        navigator.navigateToDetailsActivity(this,show,false)
    }

    override fun showLoad() {
        loading.visibility = View.VISIBLE
        tvMazeShows.visibility = View.GONE
        noNetwork.visibility = View.GONE
    }

    override fun showData() {
        loading.visibility = View.GONE
        tvMazeShows.visibility = View.VISIBLE
        noNetwork.visibility = View.GONE
    }

    override fun showNoNetwork() {
        loading.visibility = View.GONE
        tvMazeShows.visibility = View.GONE
        noNetwork.visibility = View.VISIBLE
    }
}
