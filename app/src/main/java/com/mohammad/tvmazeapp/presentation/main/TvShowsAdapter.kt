package com.mohammad.tvmazeapp.presentation.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mohammad.tvmazeapp.R
import com.mohammad.tvmazeapp.presentation._common.models.Show
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.show_item.view.*

class TvShowsAdapter(private val shows: List<Show>, val listener: TvShowsAdapterInterface): RecyclerView.Adapter<TvShowsAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.show_item,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = shows.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Picasso.get()
            .load(shows[position].image?.medium)
            .placeholder(R.drawable.show_placeholder)
            .into(holder.showImage)
        holder.showTitle.text = shows[position].name
        holder.itemView.setOnClickListener {
            listener.onItemClick(position)
        }
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val showImage: ImageView = itemView.showImage
        val showTitle: TextView = itemView.showTitle
    }

    interface TvShowsAdapterInterface{
        fun onItemClick(position: Int)
    }
}