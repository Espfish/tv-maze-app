package com.mohammad.tvmazeapp.presentation._common

import android.annotation.SuppressLint
import android.view.MenuItem
import com.mohammad.tvmazeapp.framework.Navigator
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject


@SuppressLint("Registered")
open class BaseActivity: DaggerAppCompatActivity(){
    @Inject
    lateinit var navigator: Navigator

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val itemId: Int = item?.itemId?:0
        if (itemId == android.R.id.home) {
            onBackPressed()
        }
        return true
    }

}