package com.mohammad.tvmazeapp.presentation.main

import com.mohammad.tvmazeapp.framework.api.ApiShow
import com.mohammad.tvmazeapp.framework.api.ApiWrapper
import com.mohammad.tvmazeapp.presentation._common.models.Show
import com.mohammad.tvmazeapp.presentation._common.rxjava.ApiListener

interface MainContract {
    interface View{
        fun initLayout()
        fun loadData(shows: List<Show>)
        fun navigateToDetailsScreen(show: Show)
        fun showLoad()
        fun showData()
        fun showNoNetwork()

    }

    interface Presenter{
        fun onCreate()
        fun onSearch(search: String)
        fun onDestroy()
        fun onItemClicked(position:Int)
        fun bindTodaysShows()
        fun retryClicked()
    }

    interface Interactor{
        fun getShowsForDate(date: String, listener:  ApiListener<List<ApiWrapper>>)
        fun searchShow(search: String, listener:  ApiListener<List<ApiWrapper>>)
    }
}