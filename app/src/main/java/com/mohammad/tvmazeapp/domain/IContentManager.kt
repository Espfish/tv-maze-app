package com.mohammad.tvmazeapp.domain

import com.mohammad.tvmazeapp.framework.api.ApiWrapper
import io.reactivex.SingleObserver

interface IContentManager {
    fun getShowsForDate(date: String, listener: SingleObserver<List<ApiWrapper>>)
    fun searchShowByName(q: String, listener: SingleObserver<List<ApiWrapper>>)
}