package com.mohammad.tvmazeapp.domain

interface ILogger {
    fun d(msg: String)
    fun d(t: Throwable)

    fun e(msg: String)
    fun e(t: Throwable)
}